using System.Runtime;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

/*
var exempl = {"name":"Afghanistan","alpha-2":"AF","alpha-3":"AFG","country-code":"004","iso_3166-2":"ISO 3166-2:AF",
"region":"Asia","sub-region":"Southern Asia","intermediate-region":"","region-code":"142","sub-region-code":"034",
"intermediate-region-code":""};
*/

[System.Serializable]
public class Country
{
    //these variables are case sensitive and must match the strings in the JSON.
    public string name;
    public string alpha_2;
    public string alpha_3;
    public string country_code;
    public string iso_3166_2;
    public string region;
    public string sub_region;
    public string intermediate_region;
    public string region_code;
}
 

[System.Serializable]
public class Countries
{
    public List<Country> countries = new List<Country>();
}

public class DataGenerator : MonoBehaviour
{
    public TextAsset jsonCountryFile;
    public Countries countries;
    public int[] portValues = { 22, 23, 80, 8080, 3389 };

    public  int SourceMin = 1, SourceMax = 10;
    public float SizeMin = 0.2F, SizeMax = 8.5F;

    // Start is called before the first frame update
    void Start()
    {
        countries = JsonUtility.FromJson<Countries>(jsonCountryFile.text);

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public LHSData getData()
    {
        LHSData lhsData = new LHSData();
        var targetApps = lhsData.targetApps;

        // In case of country list is not loaded yet
        if (countries.countries.Count == 0) {return lhsData;}

        // Generate a list of attacks for each port
        for (int i = 0; i < portValues.Length; i++) {
            TargetApp target = new TargetApp();
            target.port = portValues[i];
            int sources = UnityEngine.Random.Range(SourceMin, SourceMax);

            // Generate a list of attackants sources
            for (int j = 0; j < sources; j++) {
                Source source = new Source();
                int country_id = UnityEngine.Random.Range(0, countries.countries.Count);

                // Attackant source's country
                Country country = countries.countries[country_id];
                source.country_code2 = country.alpha_2;
                source.country_name = country.name;
                // Number of attacks from this country
                source.country_count =(int) Math.Exp(UnityEngine.Random.Range(SizeMin, SizeMax));

                target.sources.Add(source);
            }
            targetApps.Add(target);
        }
        return lhsData;
    }
}
