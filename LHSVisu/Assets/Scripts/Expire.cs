using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("Expiration")]
public class Expire : MonoBehaviour
{

    [SerializeField, Tooltip("Expiration delay")]
    public float expirationTime = 20f;

    private float startTime;

    // Start is called before the first frame update
    void Start()
    {
        startTime = Time.time;
    }

    // Update is called once per frame
    void Update()
    {

        if (transform.position.y < -10)
        {
            Debug.Log("Sphere too low");
            Destroy(gameObject);
            return;
        }
        // --- expired ---
        if ((Time.time - startTime) > expirationTime)
        {
            GameObject[] spheres = GameObject.FindGameObjectsWithTag("balls");
            if (spheres.Length < 20)
            {
                startTime += 10;
                return;
            }
            Destroy(gameObject);
        }
    }
}
