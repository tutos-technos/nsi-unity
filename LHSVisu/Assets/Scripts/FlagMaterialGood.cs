using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

[RequireComponent(typeof(Renderer))]
[AddComponentMenu("Good Country flag")]
public class FlagMaterialGood : MonoBehaviour
{
    private string urlBase = "https://flagcdn.com/256x192/";

    private Material flagMat = null;
    // Start is called before the first frame update
    void Start()
    {
    }


    public void setFlag(string country)
    {
        Renderer flagRenderer = GetComponent<Renderer>();
        if (flagRenderer != null) flagMat = flagRenderer.material;
        if (flagMat == null) return;
        
        StartCoroutine(DownloadImage(urlBase + country.ToLower() + ".png"));
    }

    IEnumerator DownloadImage(string MediaUrl)
    {

        UnityWebRequest request = UnityWebRequestTexture.GetTexture(MediaUrl);
        yield return request.SendWebRequest();

        // Please fill here :)
        if (request.result == UnityWebRequest.Result.Success)
        {
            // Debug.Log("Download : " + MediaUrl);

            Texture myTexture = DownloadHandlerTexture.GetContent(request);
            flagMat.SetTexture("_MainTex", myTexture);
            flagMat.SetTextureScale("_MainTex", new Vector2(2f, 2f));
        }

    }
}
