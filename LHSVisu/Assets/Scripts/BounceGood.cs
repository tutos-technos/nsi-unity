using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[AddComponentMenu("BounceGood object")]

public class BounceGood : MonoBehaviour
{

    [SerializeField, Tooltip("bounce factor")]
    public float forceFactor = 20f;

    [SerializeField, Tooltip("Sound when hit")]
    public AudioClip hitSoundClip = null;

    private AudioSource sound;
    private Rigidbody rb;


    // Start is called before the first frame update
    void Start()
    {

        rb = gameObject.GetComponent<Rigidbody>();
        sound = gameObject.GetComponent<AudioSource>();
        if (sound)
        {
            sound.loop = false;
            sound.playOnAwake = false;
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnCollisionEnter(Collision collision)
    {
        float force = collision.relativeVelocity.magnitude > 10f ? 10f : collision.relativeVelocity.magnitude;
        if (force < 2) return;

        // If bounce with another elastic object, avoid jump to fast
        Bounce other = collision.gameObject.GetComponent<Bounce>();
        float f = other ? forceFactor / 4 : forceFactor;

        // Do the bounce
        // Debug.Log("Add bounce "+force);
        rb.AddForce(collision.contacts[0].normal * (force * f));

        // Play the sound
        if (hitSoundClip == null) return;
        if (sound == null) return;
        sound.volume = force / 10f;
        sound.PlayOneShot(hitSoundClip);


    }
}
