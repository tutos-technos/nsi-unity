using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[AddComponentMenu("Spawn")]
public class Spawn : MonoBehaviour
{

    [SerializeField, Tooltip("Object to spawn")]
    public GameObject srcObject;
    [SerializeField, Tooltip("Target to throw")]
    public Vector3 ThrowTarget = new Vector3(0, 20, 0);

    [SerializeField, Tooltip("throw force")]
    public float Force = 6f;

    private int index = 0;

    private Vector3 direction;

    // Start is called before the first frame update
    void Start()
    {
        direction = Vector3.Normalize(ThrowTarget - transform.position);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SpawnObject(int size, string country, string countryName, int port)
    {
        if (!srcObject) return;

        GameObject no = Instantiate(srcObject, this.transform);
        no.transform.position = this.transform.position;
        no.name = srcObject.name+"_"+gameObject.name+"_"+index++;

        no.SetActive(true);
    
        // Set the scale relative to the size of the attack
        float s = Mathf.Log(size, 20);
        if ( s <= 0.6 ) s = 0.6f;

        no.transform.localScale = new Vector3(s, s, s);


        // Set the flag to the country
        no.GetComponent<FlagMaterial>().setFlag(country);

        // Set the attraction beetwen balls
        no.GetComponent<Attraction>().setValues(port, size);

        // Set the same parent
        no.transform.SetParent(srcObject.transform.parent);


        // Set the text with the country
        for (int i = 0; i < no.transform.childCount; i++)
        {
            Transform t = no.transform.GetChild(i);
            GameObject sub = t.gameObject;
            if ( sub.activeSelf == false ) continue;
            if (sub.name == "CountryName") sub.GetComponent<FixedText>().SetText(countryName);
            if (sub.name == "PortName") sub.GetComponent<FixedText>().SetText(""+port);
        }

        // Add the force 
        Rigidbody rb = no.GetComponent<Rigidbody>();
        if (rb)
        {
            rb.AddForce(direction * Force);
        }

    }
}
