---
title: "Atelier NSI : Unity 3D"
tags: tutotechno, unity, game, jeux, VR, NSI
date: April 05, 2023
author: Laurent PIERRON (Laurent.Pierron@inria.fr)
theme: sky
margin: 0.1
center: false
width: 960
height: 700
---

## NSI : Atelier Unity 3D


### Objectif

[https://tutos-technos.gitlabpages.inria.fr/nsi-unity](https://tutos-technos.gitlabpages.inria.fr/nsi-unity)

Représenter les données d'analyse d'intrusion du Laboratoire de Haute Sécurité (LHS) sous la forme de sphères rebondissantes.

![](./images/LHS_Visu_001.png)

### Cahier des charges

* tentatives d'intrusion en continu format *JSON*
* tentative (port, pays origine) représentée par une sphère
	* taille = nombre d'essais
	* couleur = drapeau du pays
* comportement des sphères
	* apparition dans une boîte
	* rebondissantes
	* groupées par numéro de port
	* disparition temporisée
	
::: notes
* Un serveur fournit en continu au format JSON des données synthétiques de tentative d'intrusion sur un serveur *HoneyPot*
* Chaque tentative d'intrusion sur un port réseau depuis une source (définie par un pays) sera représenté par une sphère :
    * la taille de la sphère est proportionnelle au nombre de tentatives
    * la sphère est décorée avec le drapeau du pays d'origine de l'attaquant
* Les boules apparaissent dans une boîte quand une tentative d'intrusion est détectée
* Les boules rebondissent les unes avec les autres
* Les boules avec les mêmes numéros de port s'attirent, sinon elles se repoussent
* Les boules disparaissent au bout d'un moment
:::

<!-- insérer une vidéo -(https://unitytech.github.io/unity-recorder/manual/index.html) --> 


## Unity qu'est-ce ?

### Carte d'identité

* date de naissance : Juin 2005 à Copenhague
* domicile : San Francisco, Californie
* employés : 7 703 (2022)
* chiffre d'affaires : 1,390 milliards $
* bénéfices : -921 millions $
* utilisateurs : 2 milliards, dont 1,5 millions créateurs

### Environnement Unity 3D

* **moteur de jeu** : simulateur temps réel de monde imaginaire en 2D ou 3D
* **multiplateforme** (25) : consoles, PC, smartphone, VR/AR
* **écosystème** :
  * éditeur de monde : *Unity Editor* (MacOS, Windows, Linux)
  * gestion de projets : *Unity Hub*
  * documentation : [manuel](https://docs.unity3d.com/2021.3/Documentation/Manual/UnityManual.html) et interface de programmation [API](https://docs.unity3d.com/2021.3/Documentation/ScriptReference/index.html)
  * [didacticiels](https://learn.unity.com) et sites communautaires
  * magasin de composants : [Assets Store](https://assetstore.unity.com)

::: notes

**Unity** est un moteur de jeu multiplateforme (smartphone, ordinateur, consoles de jeux vidéo, Web, casques VR/AR, etc.) développé par Unity Technologies.

Unity Technologies fournit un environnement de développement intégré (IDE), version gratuite limitée, ainsi qu'une multitude de bibliothèques logicielles et d'outils d'édition, d'import et d'export de données.

Une application peut être totalement développée avec l'IDE, mais si l'on souhaite aller plus loin il faut  programmer des objets en langage C#.

On pense souvent à tort que cette technologie est dédiée aux jeux vidéo ; elle permet cependant de créer des expériences 2D ou 3D à destination d’un ensemble de plateformes dont le mobile. Ici, Le terme expérience à son importance et englobe à la fois applications, jeux vidéo, réalité virtuelle et augmentée, animation
:::

### Rendu graphique

Unity gère le rendu 2D et 3D à travers plusieurs moteurs (URP, HDRP, Build-in)

* caméras
* éclairage : lumière, ombre
* maillages, textures, matières, ombrages
* effets visuels ( brillance, brouillard, flou, particules, etc.)
* ciel, etc.

    [Manual:Graphics](https://docs.unity3d.com/2021.3/Documentation/Manual/Graphics.html)

### Lois de la mécanique

Unity gère les lois de la mécanique (*physics* en anglais) en 2D et 3D :

* force : gravitation, collision
* mouvement : vitesse, accélération
* segments rigides reliés, marionnettes (*ragdoll*)
* mouvement des vêtements
* *pas de mécanique des fluides :-(*)

<a href="https://docs.unity3d.com/2021.3/Documentation/Manual/PhysicsSection.html" target="_blank">Manual:PhysicsSection</a>

### Jouabilité / Interaction

* Animation des objets (mouvement, transformations) avec machine à états
* Interface utilisateur (UI)
* Déplacement intelligent des personnages (IA)
* Entrées du joueur (souris, clavier, manettes, écran tactile, accéléromètre, VR/AR contrôleurs,..)
* Multi-joueur et réseau
* ...

<a href="https://docs.unity3d.com/2021.3/Documentation/Manual/AnimationSection.html" target="_blank">Manual:AnimationSection</a>

### Mais aussi...

* Monde 3D : terrain, arbres, ciel
* Audio (bruit, musique, son directionnel)
* Vidéos
* Magasin d'*Assets*
* *Prefab* objets
* Intégration d'objets 3D d'autres outils (blender, Solidworks, URDF Robotic, ...)
* Système de greffons (plug-ins)
* ...

### Programmation

* comportement des objets :
  * composants (*Compoent*) ajoutés aux objets (*GameObject*) dans l'éditeur
  * scripts (*Scripts*) : sous-classe C# de *MonoBehaviour*

* moteur de jeu :
  * boucle infinie temporisée (*PlayerLoop*)
  * envoi évènements à tous les objets actifs de la scène

<a href="https://docs.unity3d.com/2021.3/Documentation/Manual/ScriptingSection.html" target="_blank">Manual:ScriptingSection</a>

<a href="https://docs.unity3d.com/2021.3/Documentation/Manual/ExecutionOrder.html" target="_blank">Manual:ExecutionOrder</a>

---

<!-- ![](./images/monobehaviour_flowchart.svg) -->
<img src="./images/monobehaviour_flowchart.svg"  height="700">

::: notes
Le comportement des objets (*GameObject*) dans une scène est contrôlé par les composants (*Component*), qui y sont attachés. 
Bien que les composants fournis ou ajoutés depuis le magasin (*Asset store*)
sont très riches et versatiles. 
Il arrivera un moment où vous souhaiterez avoir un comportement et des interactions couvrant vos propres besoins, alors vous devrez créer vos propres composants en C#.

La style de programmation est orientée objet. Chaque élément dans une scène est une instance de la classe *GameObject*.

Les programmes appelés *scripts* sont également des objet de la classe *MonoBehaviour* et répondent à de nombreux messages (65) : *Start(), Update(), OnMouseDown(), etc.*

Pour être utilisés par le moteur de jeu, les *scripts* doivent être attachés à un objet *GameObject*.

Les principales classes fournies par *Unity* sont : *Transform, Vector, Quaternion, ScriptableObject, Time, Mathf, Random, Debug, Gizmos 
:::

## Installation du projet

### Développement environnement

Prérequis :

1. *Unity Hub* : https://unity.com/download
1. *Unity Editor* **2021.3.22f1** : installé depuis *Unity Hub*
1. Code C# IDE (**Visual Studio Code**) ou un autre éditeur (Notepad++, etc.) : (Scripting Tools)[https://docs.unity3d.com/Manual/ScriptingToolsIDEs.html]
1. Lier *Unity Editor* and *visual studio* : 
   `Edit -> Preference -> External tools -> External Studio Code`

::: notes
Il est possible d'utiliser un autre éditeur : Notepad+++, Geany, etc.

L'éditeur doit avoir un mode pour éditer des fichiers C#
:::

### Téléchargement du projet

```bash
git clone https://gitlab.inria.fr/tutos-technos/nsi-unity.git
```
ou une *release* : https://gitlab.inria.fr/tutos-technos/nsi-unity/-/releases

1. Start Unity Hub
1. In **Projects** tab, click the arrow near *Open* and *Add project from disk*
1. Browse to: **nsi-unity/LHSVisu**
1. Click *Add project*
1. Double click on the project in the list.
1. Link **Unity Editor** and **Visual Studio** : *Edit -> Preference -> External tools -> External Studio Code*

### Ouvrir la scène principale

1. *File -> Open Scene*
1. Select **Assets/Scenes/SampleScene.unity**

Should looks like

![](./images/samplescene.png)

::: notes

La scène contient :
- un sol, 4 murs et un ciel
- une source de lumière
- une caméra que l'on ne voit pas

:::

## Premiers pas

### L'interface utilisateur

Nombreuses vues :

| | |
| -- | -- |
| *Scene* | the current scene in 3D |
| *Hierachy* | The list of **GameObject** |
| *Project* | File manager in the project |
| *Inspector* | Detail of the current selected GameObject |
| ... | *Console*, *Game*, *Animations*, *Animator controler*... |

Un tas de façon pour déplacer, ajouter, supprimer, détacher des vues.

### GameObject

* Tout est un **Gameobject** (camera, objects, lights, ...)
* Un **GameObject** peut être parent de plusieurs **GameObject**
* Un **GameObject** contient des **Component**
* Un **GameObject** appartient à une **Scene** 

Regarder la vue *Hierarchy*

```mermaid
flowchart LR;
    s1(scene 1)-->g1(GameObject 1);
    s1(scene 1)-->g4(GameObject 4);
    g1-->g2(GameObject 2);
    g1-->g3(GameObject 3);
    g1-->c1([component 1]);
    g1-->c2([component 2]);
    g2-->c21([component 3]);
    g2-->c22([component 4]);
    s2(scene 2)-->g5(GameObject 5);
```

### Types de *GameObject*

| | |
| -- | -- |
| *3D Object* | A list of cubes, spheres, plane, text... |
| *Effects* | particles |
| *Lights* | Different kind of lights |
| *Audio* | Audio Sources |
| *Video* | Video Sources |
| *UI* | 2D buttons, text, checkboxes, ... |
| *Camera* | A camera ! |

<span class="fragment fade-in">
  <span class="fragment highlight-red">
    <span class="fragment fade-out">
      **Exercice:** Ajouter un nouveau **GameObject** *capsule*, changer taille et position...exécuter
    </span>
  </span>
</span>

### Composants : *Component*

Tout est dans la vue *Inspector* d'un *GameObject*

Vous pouvez ajouter et supprimer des composants, changer l'ordre

![](./images/inspector.png)

### Composant *Transform*

*Transform* contient la position, taille et rotation

![](./images/transform.png)


### Composant *Mesh* 

*Mesh* est la définition du maillage (sommets, arêtes et faces) de l'objet

![](./images/mesh.png)

### Composant *Box collider*

*Box collider* décrit la collison entre les objets

![](./images/collider.png)

### Composant *RigidBody* 

Un *RigidBody* donne de la masse et de l'attarction à un objet

![](./images/rigidbody.png)


<span class="fragment fade-in">
  <span class="fragment highlight-red">
    <span class="fragment fade-out">
        **Exercice:** Ajouter un *RigidBody* à la **capsule**...et zou!
    </span>
  </span>
</span>

## Scripts

### C#

> ...Object oriented, strongly typed...

* The worst from java
* The bad stuff from C++

```csharp
using System;

public class HelloWorld
{
    public static void Main(string[] args)
    {
        Console.WriteLine("Hello world!");
    }
}
````

### scripts as components

A script **is** a *Component* attached to a *GameObject*

* A script is related to this *GameObject*.
* A script can access to all *Component* of this *GameObject*.
* A script can access to all *GameObject* of the *Scene*.
* A script can access to all *Component* of all *GameObject*.

### MonoBehaviour

```csharp
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("HelloWord")]

public class HelloWord : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("yeah !!!");
    }
    // Update is called once per frame
    void Update()
    {
    }
}
```

<span class="fragment fade-in">
  <span class="fragment highlight-red">
    <span class="fragment fade-out">
        **Exercice:** Add this script to the **capsule**...then run
    </span>
  </span>
</span>

### Déclencheurs *MonoBehaviour*

Beaucoup de propriétés et de déclencheurs

[Lets see the doc](https://docs.unity3d.com/ScriptReference/MonoBehaviour.html)


<div class="fragment fade-in">
> Modifier le script pour avoir une message lors d'une collision
</div>


### Attributs C# utiles pour Unity

<div class="container" style="display:flex">

<div class="col" >

```csharp
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("Attraction")]
[RequireComponent(typeof(Rigidbody))]

public class Attraction : MonoBehaviour
{
    [SerializeField, Tooltip("Attraction factor")]
    public float AttractionFactor = 1;
    [SerializeField, Tooltip("Minimum distance")]
    public float MinDistance = 1;
    [SerializeField, Tooltip("Maximum Force")]
    public float MaxForce = 1;
    public int port = 0;
    // Start is called before the first frame update
    public int count = 0;

```

</div>

<div class="col">
![](./images/attraction.png)
</div>

</div>

## Tuto
    
### Sequences de scripts

```mermaid
flowchart LR;
    mm(Master)-->Master([Master]);
    Master-->spawnArea(spawnArea);
    spawnArea-->Spawn([Spawn]);
    Spawn-->Sphere
    Sphere-->attraction([Attraction])
    Sphere-->expire([Expire])
    Sphere-->bounce([Bounce])
    Sphere-->flag([FlagMaterial])
    Sphere-->CountryName(CountryName)
    Sphere-->PortName(PortName)
    CountryName-->FixedText1([FixedText])
    PortName-->FixedText2([FixedText])
```

```mermaid
flowchart LR;
    gg(GameObject)-->cc([Script])
```

### Les scripts 1/2

| | |
| -- | -- |
| *Master* | Get infos from HSL, and lauch *Spawn* |
| *Spawn* | Create and throw a Sphere, change its CountryName and PortName |
| *Attraction* | Compute attractions between spheres and add a force to the sphere |
| *Expire* | Destroy the sphere after a while |


### Les scripts 2/2

| | |
| -- | -- |
| *Bounce* | Compute the Bounce and add a force to the sphere |
| *FlagMaterial* | Download the `png` flag from internet and apply to the sphere's material. |
| *Fixed Text* | Set the text right to the camera |

### EXO #1: Rotation animée de la caméra

* Script : CameraMove.cs
* Modification : à chaque moment faire tourner la caméra en filmant le centre de la scène
* astuces :
  * class [*Transform*](https://docs.unity3d.com/ScriptReference/Transform.html) : *RotateAround*, *LookAt*
  * class *Vector3* (https://docs.unity3d.com/ScriptReference/Vector3.html) : *zero*, *up*
  * class *Time* : *deltaTime*

### EXO #2: Ajouter du son au rebondissement

* Script : Bounce.cs
* Modification : appliquer un son quand la balle touche une surface
* aide :
      * voir *AudioSource*, *PlayOneShot*

### EXO #1: Afficher le drapeau

* Script : FlagMaterial.cs
* Modification : afficher le drapeau du pays sur la sphère
* aide :
      * see *DownloadHandlerTexture*, *Renderer*, *SetTexture*, *Material*, *"_MainTex"*

## Conclusion

### La seule limite est votre imagination

Une bonne application pour faire d'incroyable simulation !

![](./images/OculusScreenshot.jpeg)
