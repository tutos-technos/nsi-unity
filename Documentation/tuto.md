---
title: Unity3D Tutotechno
tags: tutotechno, unity, game, jeux, VR
date: March 01, 2022
theme: league
margin: 0.3

---

## The tutotechno


### The purpose


HSL ( High Security Lab Scan ) represented as Balls.

Rules :

* Each ball represents a scan (a port number).
  * Size of the ball according to the number of attempts
  * painted with flag of source country
* Balls are thrown in a box when scan appear
* Balls bump each other
* Attractions between balls with the same port number
* repulsion otherwise
* Balls disappear after a while

---

![](./images/LHS_Visu_001.png)


## What is unity ?


### Game engine

Unity is a cross-platform game engine developed by Unity Technologies, first announced and released in June 2005.

The engine can be used to create three-dimensional (3D) and two-dimensional (2D) games, as well as interactive simulations and other experiences like VR (virtual reality).

### Rendering

Unity manage the rendering :

* Cameras
* lighting, shadowing, precomputed
* Render Pipeline, URP, HDRP, SRP
* Textures
* Shaders (existing, specific)
* Materials
* Visual effects (bloom, Fog, ...)
* ...

### A physical engine

Unity manage the physics :

* Gravity, mass, forces, grip
* Collision detection, Colliders, Rigidity
* Joints, Hinge, Springs, Articulation (for bodies)
* World (terrain, trees), sky
* Clothes, wind, explosion, ...
* ...

### Gaming

Unity manage :

* Ragdoll and characters (running, jumping, ...)
* Animation of objects (movement, transformations)
  * Animation clip
  * Animation Controler
* User interface (UI)
* Navigation and path finding (IA)
* Player input (keyboard, sticks, XR)
* Multiplayer and networking
* ...

### And others

Unity manage too :

* Audio (noises, music, oriented sound)
* Videos
* Assets store
* Prefab objects
* Integration of 3D objects from other tools (blender, Solidworks, URDF Robotic, ...)
* ...

## Installing

### Installing development environment

Prerequisites:

1. Unity Hub : https://unity.com/download
1. Unity Editor **2020.3.12f1** : installed from Unity Hub
1. IDE (**Visual Studio Code**) : https://docs.unity3d.com/Manual/ScriptingToolsIDEs.html
1. Link Unity Editor and visual studio : Edit -> Preference -> External tools -> External Studio Code

### Starting the project

```bash

git clone git@gitlab.inria.fr:wallrich/tutounity.git

```

Steps:

1. Start Unity Hub
    1. Open projet
    1. Choose : tutounity/LHSVisu
    1. Click Open

Now your project is open in Unity IDE.

Link Unity Editor and visual studio : Edit -> Preference -> External tools -> External Studio Code

### Importing the scene

Steps :

1. File -> Open scene
1. Select **SampleScene.unity**

Shoud looks like :

![](./images/samplescene.png)


## First steps

### The GUI

Several panels :

| | |
| -- | -- |
| *Scene* | the current scene in 3D |
| *Hierachy* | The list of **gameObjects** |
| *Project* | File manager in the project |
| *Inspector* | Detail of the current selected gameObject |
| ... | *Console*, *Game*, *Animations*, *Animator controler*... |

A lot of way to move, add, delete, attach, detach panels... 

### GameObjects

* All stuffs are **gameobjects** (camera, objects, lights, ...)
* A **gameObject** can be a parent of **gameObjects**.
* A **gameObject** contains several **components**
* A **gameObject** belongs to a **Scene**

See in the hierarchy panel

```mermaid
flowchart LR;
    s1(scene 1)-->g1(gameObject 1);
    s1(scene 1)-->g4(gameObject 4);
    g1-->g2(gameObject 2);
    g1-->g3(gameObject 3);
    g1-->c1([component 1]);
    g1-->c2([component 2]);
    g2-->c21([component 3]);
    g2-->c22([component 4]);
    s2(scene 2)-->g5(gameObject 5);
```

### GameObject style

a lot of **gameObjects**

| | |
| -- | -- |
| *3D Object* | A list of cubes, spheres, plane, text... |
| *Effects* | particles |
| *Lights* | Different kind of lights |
| *Audio* | Audio Sources |
| *Video* | Video Sources |
| *UI* | 2D buttons, text, checkboxes, ... |
| *Camera* | A camera ! |


<div class="fragment fade-in">
> Add a new gameObject cube, change its size, position...then run
</div>


### Components

Everything is in the *Inspector* panel
You can add and remove components, change there order

![](./images/inspector.png)

### Transform component

Transform is the position, size and rotation

![](./images/transform.png)


### Mesh component

Mesh is the mesh definition of the object

![](./images/mesh.png)

### Box collider component

Box collider is the way to manage collision between objects

![](./images/collider.png)

### RigidBody component

A rigidBody give mass, gravity to an object

![](./images/rigidbody.png)


<div class="fragment fade-in">
> Add a rigidbody to the cube...then run
</div>

## Scripts

### C sharp

> ...Object oriented, strongly typed...

* The worst from java
* The bad stuff from C++

```C#
using System;

public class HelloWorld
{
    public static void Main(string[] args)
    {
        Console.WriteLine("Hello world!");
    }
}
```

### scripts as components

A script **is** a component attached to a gameObject.

* A script is related to this gameObject.
* A script can access to all components of this gameObject.
* A script can access to all gameObjects of the scene.
* A script can access to all components of all gameObjects.

### MonoBehaviour

```C#
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("HelloWord")]

public class HelloWord : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("yeah !!!");
    }
    // Update is called once per frame
    void Update()
    {
    }
}
```

<div class="fragment fade-in">
> Add this script to the cube...then run
</div>

### MonoBehaviour triggers

A lot of properties and triggers

[Lets see the doc](https://docs.unity3d.com/2021.2/Documentation/ScriptReference/MonoBehaviour.html)


<div class="fragment fade-in">
> Modify the script to get a message when collide
</div>


### Usefull C Sharp attributes for Unity

<div class="container" style="display:flex">

<div class="col" >

```C#
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("Attraction")]
[RequireComponent(typeof(Rigidbody))]

public class Attraction : MonoBehaviour
{
    [SerializeField, Tooltip("Attraction factor")]
    public float AttractionFactor = 1;
    [SerializeField, Tooltip("Minimum distance")]
    public float MinDistance = 1;
    [SerializeField, Tooltip("Maximum Force")]
    public float MaxForce = 1;
    public int port = 0;
    // Start is called before the first frame update
    public int count = 0;

```

</div>

<div class="col">
![](./images/attraction.png)
</div>

</div>

## Tuto

### Sequences de scripts


```mermaid
flowchart LR;
    mm(Master)-->Master([Master]);
    Master-->spawnArea(spawnArea);
    spawnArea-->Spawn([Spawn]);
    Spawn-->Sphere
    Sphere-->attraction([Attraction])
    Sphere-->expire([Expire])
    Sphere-->bounce([Bounce])
    Sphere-->flag([FlagMaterial])
    Sphere-->CountryName(CountryName)
    Sphere-->PortName(PortName)
    CountryName-->FixedText1([FixedText])
    PortName-->FixedText2([FixedText])
```

```mermaid
flowchart LR;
    gg(GameObject)-->cc([Script])
```

### Les scripts 1/2

| | |
| -- | -- |
| *Master* | Get infos from HSL, and lauch *Spawn* |
| *Spawn* | Create and throw a Sphere, change its CountryName and PortName |
| *Attraction* | Compute attractions between spheres and add a force to the sphere |
| *Expire* | Destroy the sphere after a while |


### Les scripts 2/2

| | |
| -- | -- |
| *Bounce* | Compute the Bounce and add a force to the sphere |
| *FlagMaterial* | Download the `png` flag from internet and apply to the sphere's material. |
| *Fixed Text* | Set the text right to the camera |

### Set the camera rotation

* Script : CameraMove.cs
* Modification : rotate the camera and look at the center of the scene
* help :
  * see *Vector3*, *transform*, *RotateAround*, *Time.deltaTime*, *LookAt* 

### Add sound to the bounce

* Script : Bounce.cs
* Modification : apply a sound when a ball hit a surface
* help :
  * see *AudioSource*, *PlayOneShot*

### Apply the flag material

* Script : FlagMaterial.cs
* Modification : apply the flag to the material
* help :
  * see *DownloadHandlerTexture*, *Renderer*, *SetTexture*, *Material*, *_MainTex*

## Conclusion

### The only limit is your imagination

A good way to make awesome demonstration !

![](./images/OculusScreenshot.jpeg)
